module Data.Colour.Extra where

import Data.Colour.SRGB as RGB
import qualified SDL 
import Data.Word

type Color = Colour Double
type SDLColor = SDL.V4 Word8

convertRGB :: Colour Double -> SDLColor
convertRGB color = case triple of RGB r g b -> SDL.V4 r g b 255
  where
    triple = RGB.toSRGB24 color