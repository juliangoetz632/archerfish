{-# LANGUAGE TypeSynonymInstances #-} -- newtypes definieren -> neuer Konstruktor
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Dimensions
    (
        module Linear.Metric,
        module Linear.Vector,
        module Linear.V2,
        Vector2,
        Position,
        Size,
        Velocity,
        position,
        size,
        velocity,
        v2x,
        v2y,
        vnormalize
    ) where

import Linear.Epsilon
import Linear.Vector
import Linear.Metric
import Linear.V2
import FRP.Yampa.VectorSpace as Yampa

type Vector2 = V2
type Position = Vector2 Double --falsche Benutzung wird nicht erkannt -> newtype
type Size = Vector2 Double

type Velocity = Vector2 Double

--TODO kann Dimensions module *^ etc von Linear package exportieren?

--TODO: OrphanInstance, in Extensionmodule deklarieren
instance RealFloat a => VectorSpace (V2 a) a where
    zeroVector = zero

    a *^ v = a Linear.Vector.*^ v 

    v ^/ a = v Linear.Vector.^/ a

    negateVector v = Linear.Vector.negated v

    v1 ^+^ v2 = v1 Linear.Vector.^+^ v2

    v1 ^-^ v2 = v1 Linear.Vector.^-^ v2

    v1 `dot` v2 = Linear.Metric.dot v1 v2

position :: Double -> Double -> Position
position x y = V2 x y

size :: Double -> Double -> Size
size x y = V2 x y

velocity :: Double -> Double -> Velocity
velocity x y = V2 x y

v2x :: RealFloat a => Vector2 a -> a
v2x (V2 a _) = a

v2y :: RealFloat a => Vector2 a -> a
v2y (V2 _ b) = b

vnormalize :: (Floating a, Epsilon a)=> V2 a -> V2 a 
vnormalize = Linear.Metric.normalize