module SDL.Extra where

import Dimensions
import SDL

pointAt :: Integral a => Position -> Point SDL.V2 a
pointAt p = case p of 
    V2 x y -> SDL.P $ SDL.V2 (round x) (round y)

vectorOf :: Integral a => Vector2 Double -> SDL.V2 a
vectorOf sz = case sz of
    V2 x y -> SDL.V2 (round x) (round y)

rect :: Integral a => Position -> Size -> Rectangle a
rect x sz = Rectangle (pointAt x) (vectorOf sz)

sdlPointToVector2 :: Integral a => SDL.Point SDL.V2 a -> Vector2 Double
sdlPointToVector2 p = case p of
  SDL.P v -> case v of
    SDL.V2 x y -> V2 (fromIntegral x) (fromIntegral y)