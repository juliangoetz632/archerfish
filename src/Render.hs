module Render where

import Data.Colour.Names as CN
import Data.StateVar
import FRP.Yampa as Yampa
import Linear.V2
import SDL ()
import qualified SDL
import SDL.Primitive as SDL

import Dimensions
import Numeric
import Objects
import Resources
import SDL.Extra
import Data.Colour.Extra

renderObjects :: SDL.Renderer -> [ObjOutput] -> IO ()
renderObjects renderer oos = 
    mapM_ (\objOutput -> renderObject renderer $ ooState objOutput) oos

renderObject :: SDL.Renderer -> ObsObjectState -> IO ()
renderObject renderer oo = renderShape renderer (oosPosition oo) (oosResource oo)

renderShape :: SDL.Renderer -> Position -> Resource Shape -> IO ()
renderShape renderer pos (Resource x) = case x of
    Rectangle {} -> renderRect renderer pos x
    Line {}  -> renderLine renderer x
    Sprite {} -> renderSprite renderer pos x
    Ellipse {} -> renderEllipse renderer pos x
    _ -> error "rendering not implemented"

renderRect :: SDL.Renderer -> Position -> Shape -> IO ()
renderRect renderer pos rect' = case rect' of
    Rectangle sz col -> renderRect' renderer pos sz $ convertRGB col
    _ -> error "no rectangle"

renderRect' :: SDL.Renderer -> Position -> Size -> SDLColor -> IO ()
renderRect' renderer p sz col = 
  SDL.rendererDrawColor renderer $= col >>= \_ ->
    SDL.fillRect renderer $ Just
    $ SDL.Rectangle (pointAt p)
    $ vectorOf sz

renderLine :: SDL.Renderer -> Shape -> IO ()
renderLine renderer res = case res of
    Line pos0 pos col -> renderLine' renderer pos0 pos $ convertRGB col
    _ -> error "no line"

renderLine' :: SDL.Renderer -> Position -> Position -> SDLColor -> IO ()
renderLine' renderer pos0 pos col =
    SDL.rendererDrawColor renderer $= col >>= \_ ->
    SDL.drawLine renderer (pointAt pos0) (pointAt pos)

renderSprite :: SDL.Renderer -> Position -> Shape -> IO ()
renderSprite renderer pos res = case res of
    Sprite sz tex flip' -> SDL.copyEx renderer tex Nothing ( Just $ rect pos sz) 0 Nothing flip'
    _ -> error "no sprite"

renderEllipse :: SDL.Renderer -> Position -> Shape -> IO ()
renderEllipse renderer pos ellipse' = case ellipse' of
    Ellipse sz col -> renderEllipse' renderer pos sz $ convertRGB col
    _ -> error "no ellipse"

renderEllipse' :: SDL.Renderer -> Position -> Size -> SDLColor -> IO ()
renderEllipse' renderer pos sz col = 
    case pos of
        V2 px py ->
            case sz of
                V2 x y ->
                    SDL.smoothEllipse renderer (V2 (round px) (round py)) (round x) (round y) col >>= \_ ->
                    SDL.fillEllipse renderer (V2 (round px) (round py)) (round x) (round y) col

renderTime :: SDL.Renderer -> Time -> Time -> IO ()
renderTime renderer current maxTime = 
    renderShape renderer (position 426 50) $ colorize CN.forestgreen <$> Resources.rectangle width 30
    where
        width = round $ interpolate id (current/maxTime) 426 0