module Input where

import qualified SDL 
import SDL.Extra
import FRP.Yampa
import FRP.Extra.Yampa

import Dimensions

data GameInput = GameInput {
  giDt :: Double,
  giMousePos :: Maybe Position,
  giMouseClicked :: Bool,
  giMouseReleased :: Bool,
  giQuit :: Bool
}

-- | Initial (default) game input.
neutralGameInput :: GameInput
neutralGameInput = GameInput{
  giDt = 0,
  giMousePos = Just $ position 0 0,
  giMouseClicked = False,
  giMouseReleased = False,
  giQuit = False
}

printMouseLocationMode :: IO ()
printMouseLocationMode = do
  mode <- SDL.getMouseLocationMode
  print $ case mode of 
    SDL.AbsoluteLocation -> "AbsoluteLocation"
    SDL.RelativeLocation -> "RelativeLocation"

getMousePressed :: SDL.Event -> Bool
getMousePressed ev = 
  case SDL.eventPayload ev of
    SDL.MouseButtonEvent eData -> SDL.mouseButtonEventMotion eData == SDL.Pressed
    _ -> False

getMouseReleased :: SDL.Event -> Bool
getMouseReleased ev = 
  case SDL.eventPayload ev of
    SDL.MouseButtonEvent eData -> SDL.mouseButtonEventMotion eData == SDL.Released
    _ -> False

-- mouse position is not scaled to sdl logical size
getMousePos :: IO Position
getMousePos = do
  p <- SDL.getAbsoluteMouseLocation
  let (x,y) = case p of SDL.P v -> case v of SDL.V2 vx vy -> (vx,vy)
  return $ position (fromIntegral x) (fromIntegral y)

keyPressed :: SDL.Event -> SDL.Keycode -> Bool
keyPressed ev key = case SDL.eventPayload ev of
  SDL.KeyboardEvent eData -> case eData of SDL.KeyboardEventData _ _ _ ks -> case ks of SDL.Keysym _ kc _ -> kc == key
  _ -> False

positionFromEvent :: SDL.Event -> Maybe Position
positionFromEvent ev = case SDL.eventPayload ev of
  SDL.MouseMotionEvent d -> Just $ sdlPointToVector2 $ SDL.mouseMotionEventPos d
  SDL.MouseButtonEvent d -> Just $ sdlPointToVector2 $ SDL.mouseButtonEventPos d
  _ -> Nothing

leftBtnPressed :: SF GameInput (Event ())
leftBtnPressed = giMouseClicked ^>> edge

pointerPos :: SF GameInput Position
pointerPos = giMousePos ^>> (holdMaybe $ position 0 0)

leftBtnReleased :: SF GameInput (Event ())
leftBtnReleased = giMouseReleased ^>> edge