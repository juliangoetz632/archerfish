module Objects where

import FRP.Yampa

import Data.IdentityList
import Dimensions
import Input
import Resources

type Object = SF ObjInput ObjOutput
type ObjOutputs = IL ObjOutput

data ObjInput = ObjInput {
  oiGameInput :: GameInput,
  oiHit :: Event ()
}

data ObjOutput = ObjOutput {
  ooState :: ObsObjectState,
  ooKillRequest :: Event (),
  ooSpawnRequest :: Event [Object],
  ooType :: ObjectType,
  ooZ :: Int -- lower means closer to camera
}

defaultObjOutput :: ObjOutput
defaultObjOutput = ObjOutput {
    ooState = undefined,
    ooKillRequest = NoEvent,
    ooSpawnRequest = NoEvent,
    ooZ = 0,
    ooType = LevelObject
}

instance Show ObjOutput where
    show oo = show $ ooState oo

data ObjectType = EnemyObject | PlayerObject | LevelObject

data ObsObjectState =
    OOSPrepareMissile {
      oosInitialPosition :: Position,
      oosPosition :: Position,
      oosSize :: Size,
      oosResource :: Resource Shape
    }
  | OOSFiredMissile {
      oosPosition :: Position,
      oosSize :: Size,
      oosResource :: Resource Shape
    }
  | OOSStaticObject {
      oosPosition :: Position,
      oosSize :: Size,
      oosResource :: Resource Shape
    }

instance Show ObsObjectState where
  show oos = case oos of 
    OOSPrepareMissile {} -> "OOSPrepareMissile"
    OOSFiredMissile {} -> "OOSFiredMissile"
    OOSStaticObject {} -> "OOSStaticObject"

oosRadius :: ObsObjectState -> Double
oosRadius x = v2x $ oosSize x

oosPrepareMissile :: Position -> Position -> Resource Shape -> ObsObjectState
oosPrepareMissile pos0 pos res = OOSPrepareMissile {
                            oosInitialPosition = pos0,
                            oosPosition = pos,
                            oosSize = size 5 5,
                            oosResource = res
                          }

oosFiredMissile :: Position -> Resource Shape -> ObsObjectState
oosFiredMissile pos res = OOSFiredMissile {
                      oosPosition = pos,
                      oosSize = size 5 5,
                      oosResource = res
                    }

oosStaticObject :: Position -> Size -> Resource Shape -> ObsObjectState
oosStaticObject pos sz res = OOSStaticObject {
                            oosPosition = pos,
                            oosSize = sz,
                            oosResource = res
                          }

isEnemy :: ObjOutput -> Bool
isEnemy oo = case ooType oo of
  EnemyObject -> True
  _ -> False