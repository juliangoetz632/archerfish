module Physics where

import Display


gravity :: Double
gravity = 240

maxHeight :: Double -> Double
maxHeight v = h
    where 
        h = (v * v) / (2*(gravity))

maxStrength :: Double --TODO: rechnet von oberer, linker Ecke des Sprites! => Mittelpunkt nutzen
maxStrength = sqrt $ 2*gravity*h
    where
        h = fromIntegral $ viewportHeight - 215

maxStrength' :: Double
maxStrength' = vz / (sin alpha)
    where
        h = 768 - 100
        w = 1280 - 100
        z = 100
        vz = sqrt $ 2*(gravity)*h - 2*(gravity)*z
        alpha = tanh $ h/w
