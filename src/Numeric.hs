module Numeric where

ease :: Floating a => a -> a
ease x = (sin (x * pi - pi/2) + 1) / 2

interpolate :: Num a => (t -> a) -> t -> a -> a -> a
interpolate d x p0 p1 = p0 + (p1 - p0)*(d x)
