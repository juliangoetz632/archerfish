module Main where


import Control.Monad
import Data.Colour.Names as CN
import Data.List
import Foreign.C
import SDL ()
import qualified SDL
import qualified SDL.Image
import System.Exit
import Data.StateVar
import Data.IORef
import FRP.Yampa as Yampa
import Display
import Input
import Time
import GameState
import Data.IdentityList
import Render
import Resources
import ObjectBehavior
import Objects
import Dimensions
import Game

main :: IO ()
main = do
  SDL.initializeAll

  _ <- SDL.setHintWithPriority SDL.OverridePriority SDL.HintRenderScaleQuality SDL.ScaleBest

  window <- createWindow "title" (CInt $ fromIntegral windowWidth) (CInt $ fromIntegral windowHeight)
  renderer <- createRenderer window

  SDL.rendererLogicalSize renderer $= (Just $ SDL.V2 (CInt $ fromIntegral viewportWidth) (CInt $ fromIntegral viewportHeight))

  SDL.showWindow window

  timeRef <- newIORef (0)

  bgRes <- sprite renderer "gradient_background.png"
  mapRes <- sprite renderer "map.png"
  mangroveRes <- sprite renderer "mangrove.png"
  playerRes <- sprite renderer "player.png"

  let
    playerPosition = position 300 $ fromIntegral $ viewportHeight - 215
    playerObj = playerObject playerPosition 0 $ playerRes
    enemies = map (\(x,y) -> enemyObject (position x y) 1 $ colorize CN.forestgreen <$> ellipse 15 15) enemyPositions

    backgroundObj = levelObject (position 0 0) (size 0 0) 6 bgRes
    mapObj = levelObject (position 0 0) (size 0 0) 5 mapRes 
    mangrove = levelObject (position 0 200) (size 0 0) 4 $ scale 0.4 <$> mangroveRes
    mangrove2 = levelObject (position 720 60) (size 0 0) 5 $ flipX <$> scale 0.30 <$> mangroveRes
    level = [backgroundObj, mapObj, mangrove, mangrove2]

    objs = (listToIL $ [playerObj] ++ enemies ++ level)

  let input _ = do
        dtSecs <- yampaSDLTimeSense timeRef
        events <- SDL.pollEvents

        let mPositions = map positionFromEvent events
        let mPosition = if null mPositions then Nothing else last mPositions
        
        let mPressed = any getMousePressed events
        let mReleased = any getMouseReleased events

        let exit = elem SDL.QuitEvent $ map SDL.eventPayload events
        when exit $ quit window renderer

        let userQuit = any (\ev -> keyPressed ev (SDL.KeycodeEscape)) events

        when mPressed $ print $ "pressed" ++ (show mPosition)
        --when mReleased $ print "released"
        
        let gi = GameInput {
          giDt = dtSecs,
          giMousePos = mPosition,
          giMouseClicked = mPressed,
          giMouseReleased = mReleased,
          giQuit = userQuit
        }

        return (dtSecs, Just gi)

  let output _ gs = do
        Display.drawBackground renderer

        renderObjects renderer sortedObjects
        renderTime renderer (gsTime gs) maximumGameLength

        SDL.present renderer
        return $ gsQuit gs
        where
          sortedObjects = reverse $ sortOn ooZ $ elemsIL$ gsObjects gs

  reactimate initialize input output (wholeGame objs)

  quit window renderer

  where
    enemyPositions = 
        [(632, 305),
        (390, 241),
        (388, 248),
        (462, 342),
        (168, 342),
        (702, 425),
        (848, 212),
        (1001, 300),
        (904, 540),
        (620, 521),
        (988, 500)]

initialize :: IO GameInput
initialize = do
  putStrLn "initialize!"
  return neutralGameInput

quit :: SDL.Window -> SDL.Renderer -> IO ()
quit window renderer 
  = do 
    putStrLn "quitting"
    SDL.destroyRenderer renderer
    SDL.destroyWindow window
    SDL.Image.quit
    SDL.quit
    exitSuccess