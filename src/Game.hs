module Game where

import FRP.Yampa as Yampa

import Dimensions
import Data.IdentityList
import GameState
import Input
import Objects


wholeGame :: IL Object -> SF GameInput GameState
wholeGame initObjs = switch
  (wholeGame' initObjs >>> identity &&& gameFinished)
  finish

gameFinished :: SF GameState (Event ())
gameFinished = noEnemies &&& timeOut >>> arr (uncurry lMerge)

finish :: () -> SF GameInput GameState
finish _ = arr (\_ -> GameState {gsObjects = emptyIL, gsQuit = True, gsTime = 30})

wholeGame' :: IL Object -> SF GameInput GameState
wholeGame' initObjs = (process initObjs &&& wantsToQuit) &&& time >>> composeGameState
  where composeGameState :: SF ((IL ObjOutput, Bool), Time) GameState
        composeGameState = arr (\((outputObjs, q),t) -> GameState {gsObjects = outputObjs, gsQuit = q, gsTime = t})

        wantsToQuit :: SF GameInput Bool
        wantsToQuit = arr(\gi -> giQuit gi)

process :: IL Object -> SF GameInput (IL ObjOutput)
process initObjs = proc input -> do
  rec
    outputObjs <- core initObjs -< (input, outputObjs)
  returnA -< outputObjs

--uses dpSwitch to maintain dynamic collection of signal functions
core :: IL Object -> SF (GameInput, (IL ObjOutput)) (IL ObjOutput)
core objs = dpSwitch route                        -- routing
                     objs                         -- init
                     (arr killAndSpawn >>> notYet) -- 
                     (\sfs' f -> core (f sfs'))   -- 

--pass on the game input to all game objects
route :: (GameInput, IL ObjOutput) -> IL obj -> IL (ObjInput, obj)
route (gi, outputs ) objs = mapIL routeAux objs
  where
    routeAux :: (ILKey, obj) -> (ObjInput, obj)
    routeAux (key, object) = (ObjInput {oiGameInput = gi,
                                        oiHit = if key `elem` hs
                                                then Event ()
                                                else NoEvent},
                              object)
    hs = hits (assocsIL (fmap ooState outputs))

--computes a list of keys of all objects that are involved in some collision
hits :: [(ILKey, ObsObjectState)] -> [ILKey]
hits keyObjOutputs = hitsAux keyObjOutputs
  where
    hitsAux [] = []
    hitsAux (x : xs) = collides x xs ++ hitsAux xs

    -- returns all the objects colliding with x1
    collides :: (ILKey, ObsObjectState) -> [(ILKey, ObsObjectState)] -> [ILKey]
    collides x1 xs = case xs of
      [] -> []
      (x2 : xs') -> collides' x1 x2 ++ collides x1 xs'

    -- checks if two objects collide with each other. If true -> returns their keys, else []
    collides' :: (ILKey, ObsObjectState) -> (ILKey, ObsObjectState) -> [ILKey]
    collides' (k1, os1) (k2, os2) = 
      if distance (oosPosition os2) (oosPosition os1) < (oosRadius os1 + oosRadius os2)
      then [k1, k2] else []


--observes output, control mechanism for adding and deleting objects
killAndSpawn :: (a, IL ObjOutput) -> Event (IL Object -> IL Object)
killAndSpawn (_, oos) =
    foldl (mergeBy (.)) noEvent es
    where
        es :: [Event (IL Object -> IL Object)]
        es = [ mergeBy (.)
                       (ooKillRequest oo `tag` (deleteIL k))
                       (fmap (foldl (.) id . map insertIL_) (ooSpawnRequest oo))
             | (k,oo) <- assocsIL oos ]