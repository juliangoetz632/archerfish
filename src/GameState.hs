module GameState where

import FRP.Yampa as Yampa

import Data.IdentityList
import Objects

data GameState = GameState {
  gsObjects :: IL ObjOutput,
  gsTime :: Double,
  gsQuit :: Bool
}

noEnemies :: SF GameState (Event ())
noEnemies = arr (\gs -> (length . filter isEnemy . elemsIL $ gsObjects gs) == 0) >>> edge

timeOut :: SF GameState (Event ())
timeOut = after maximumGameLength ()

maximumGameLength :: Time
maximumGameLength = 30