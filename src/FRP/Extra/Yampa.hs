module FRP.Extra.Yampa where

import FRP.Yampa
import FRP.Yampa.Event

--holds starting value x0 until first Just value appears. Then holds subsequent Just values
holdMaybe :: c -> SF (Maybe c) c
holdMaybe x0 = arr (maybeToEvent) >>> hold x0