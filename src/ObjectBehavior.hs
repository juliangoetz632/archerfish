{-# LANGUAGE FlexibleContexts #-}

module ObjectBehavior where

import Data.Colour.Names as CN
import FRP.Yampa

import Debug.Trace
import Dimensions
import Input
import Numeric
import Objects
import Physics
import Resources


playerObject :: Position -> Int -> Resource Shape -> Object
playerObject pos z res = proc (ObjInput {oiGameInput = gi}) -> do

    trigger <- leftBtnPressed -< gi
    pointerPosition <- pointerPos -< gi

    returnA -< ObjOutput {
                ooState = oosStaticObject pos (size 20 20) res,
                ooKillRequest = NoEvent,
                ooSpawnRequest = tag trigger [prepareMissileObject center 0],
                ooZ = z,
                ooType = PlayerObject
              }
    where
        center = case pos of V2 x y -> V2 (x + 55) (y + 40)



enemyObject :: Position -> Int -> Resource Shape -> Object
enemyObject pos z res = proc objectInput -> do

    returnA -< ObjOutput {
                ooState = oosStaticObject pos (size 20 20) res,
                ooKillRequest = oiHit objectInput,
                ooSpawnRequest = NoEvent,
                ooZ = z,
                ooType = EnemyObject
            }

debugObject :: Position -> Int -> Resource Shape -> Object
debugObject pos z res = proc _ -> do
    returnA -< defaultObjOutput {ooState = oosStaticObject pos (size 20 20) res, ooZ = z, ooType = LevelObject}


prepareMissileObject :: Position -> Int -> Object
prepareMissileObject pos0 z = proc (ObjInput {oiGameInput = gi}) -> do
    pos <- pointerPos -< gi

    fire <- leftBtnReleased -< gi

    normalizedDirection <- arr (\p -> vnormalize $ pos0 - p) -< pos

    vel <- arr (\(dir, pos) -> dir ^* strength pos0 pos) -< (normalizedDirection, pos)

    returnA -< ObjOutput {
                ooState = oosPrepareMissile pos0 pos $ colorize CN.midnightblue <$> line pos0 pos,
                ooKillRequest = fire,
                ooSpawnRequest = tag fire  [firedMissileObject pos0 vel 0 $ colorize CN.midnightblue <$> ellipse 5 5],
                ooZ = z,
                ooType = PlayerObject
            }
    where 
        minStrength = 100
        maxDistance = 160
        strength x0 x = trace ("strength: " ++ show (strength' $ t x0 x) ++ " --- distance: " ++ show (distance x0 x)) (strength' $ t x0 x)
        
        t x0 x = minimum [distance x0 x / maxDistance, 1]
        strength' t' = interpolate ease t' minStrength $ trace ("maxStrength: " ++ show maxStrength) maxStrength


firedMissileObject :: Position -> Velocity -> Int -> Resource Shape -> Object
firedMissileObject pos v0 z res = proc _ -> do

    gravityVel <- integral -< gravity

    vel <- arr (\gy -> case v0 of V2 vx vy -> velocity vx (vy+gy)) -< gravityVel

    deltaPosition <- integral -< vel

    newPosition <- arr (\dp -> pos + dp) -< deltaPosition

    returnA -< ObjOutput {
                ooState = oosFiredMissile newPosition res,
                ooKillRequest = NoEvent,
                ooSpawnRequest = NoEvent,
                ooZ = z,
                ooType = PlayerObject
            }

levelObject :: Position -> Size -> Int -> Resource Shape -> Object
levelObject pos sz z res = proc _ -> do
    returnA -< ObjOutput {
                ooState = oosStaticObject pos sz res,
                ooKillRequest = NoEvent,
                ooSpawnRequest = NoEvent,
                ooZ = z,
                ooType = LevelObject
            } 