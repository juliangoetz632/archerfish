module Resources where

import Control.Monad (liftM, ap)
import Data.Colour.Names as CN
import qualified SDL
import qualified SDL.Image

import Data.Colour.Extra
import Dimensions
import Paths_masterprojekt

newtype Resource a = Resource a

instance Functor Resource where fmap = liftM

instance Applicative Resource where  pure = return ;  (<*>) = ap

instance Monad Resource where
    return x = Resource x
    (Resource x) >>= f = f x 


data Shape = Sprite{
                 sSize :: Size,
                 sTexture :: SDL.Texture,
                 sFlip :: V2 Bool
             }
             | Rectangle{
                 sSize :: Size,
                 sColour :: Color
             }
             | Line{
                 sStart :: Position,
                 sEnd :: Position,
                 sColour :: Color
             }
             | Ellipse{
                sSize :: Size,
                sColour :: Color
             }

colorize :: Color -> Shape -> Shape
colorize col shape = shape{sColour = col}

scale :: Double -> Shape -> Shape
scale x shape = shape{sSize = sSize shape ^* x}

sprite :: SDL.Renderer -> FilePath -> IO (Resource Shape)
sprite renderer filename = do

    texture <- loadResource renderer filename

    attrs <- SDL.queryTexture texture
    let w = SDL.textureWidth attrs
        h = SDL.textureHeight attrs

    return $ Resource Sprite {
        sTexture = texture,
        sSize = size (fromIntegral w) (fromIntegral h),
        sFlip = V2 False False
    }

flipX :: Shape -> Shape
flipX shape = shape{sFlip = flipped}
    where flipped = case shape of
            Sprite _ _ f -> case f of
                V2 x y -> V2 (not x) y

rectangle :: Int -> Int -> Resource Shape
rectangle w h  = Resource Rectangle {sSize = size (fromIntegral w) (fromIntegral h), sColour = CN.black}

line :: Position -> Position -> Resource Shape
line x0 x1 = Resource Line {sStart = x0, sEnd = x1, sColour = CN.black}

ellipse :: Int -> Int -> Resource Shape
ellipse w h  = Resource Ellipse {sSize = size (fromIntegral w) (fromIntegral h), sColour = CN.black}


loadResource :: SDL.Renderer -> FilePath -> IO SDL.Texture
loadResource renderer filename =
    getDataFileName filename >>= SDL.Image.loadTexture renderer
