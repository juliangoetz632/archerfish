module Display where

import Foreign.C 
import SDL ()
import qualified SDL 
import Data.Text
import Data.StateVar
import Data.Colour.Names as ColorNames
import Data.Colour.Extra as Colors

windowWidth :: Int
windowWidth = 800

windowHeight :: Int
windowHeight = 600

viewportWidth :: Int
viewportWidth = 1280

viewportHeight :: Int
viewportHeight = 768

createWindow :: String -> CInt -> CInt -> IO SDL.Window
createWindow title width height = do
  let winConfig = SDL.defaultWindow { SDL.windowPosition = SDL.Absolute (SDL.P (SDL.V2 100 100))
                                    , SDL.windowInitialSize = SDL.V2 width height }

  SDL.createWindow (Data.Text.pack title) winConfig

createRenderer :: SDL.Window -> IO SDL.Renderer
createRenderer window = do
  let rdrConfig = SDL.RendererConfig { SDL.rendererType = SDL.AcceleratedVSyncRenderer
                                     , SDL.rendererTargetTexture = True }
  SDL.createRenderer window (-1) rdrConfig

drawBackground :: SDL.Renderer -> IO ()
drawBackground renderer = do
  let color = Colors.convertRGB ColorNames.black
  SDL.rendererDrawColor renderer $= color
  SDL.clear renderer